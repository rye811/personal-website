var name;
var numberOfPics;
var currentIndex;
var slides;

function setNameAndPicNum(newName, picNum){
  name = newName;
  numberOfPics = picNum;
  slides = new Array();
  for(j = 0; j < numberOfPics; j++){
    var img = new Image();
    img.src = name+"photos/" + (j + 1) + ".png";
    slides[j] = name+"photos/" + (j + 1) + ".png";
  }
  console.log(slides.length);
  currentIndex = 0;
}

function prevPic(){
  currentIndex--;
  if(currentIndex < 0){
      currentIndex = slides.length - 1;
  }
  document.getElementById("picture").setAttribute("src", slides[currentIndex]);
}

function nextPic(){
  currentIndex++;
  if(currentIndex == slides.length){
    currentIndex = 0;
  }
  document.getElementById("picture").setAttribute("src", slides[currentIndex]);
}
